package com.example.parcial3.model

import com.example.parcial3.R
import com.example.parcial3.databinding.ItemCountryBinding
import com.xwray.groupie.databinding.BindableItem

class CountryItem (private val country: Country, val callback: (String) -> Unit): BindableItem<ItemCountryBinding>() {

    override fun bind(viewBinding: ItemCountryBinding, position: Int) {
        viewBinding.nameTextView.text = country.name
        viewBinding.rankTextView.text = "Rango: ${country.rank}"
        viewBinding.infectionRiskTextView.text = "Infección: ${country.infectionRisk}"
        viewBinding.detailButton.setOnClickListener {
            callback(country.code)
        }
    }

    override fun getLayout(): Int {
        return R.layout.item_country
    }
}