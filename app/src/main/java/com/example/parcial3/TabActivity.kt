package com.example.parcial3

import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.parcial3.databinding.ActivityTabBinding

class TabActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityTabBinding>(this, R.layout.activity_tab)

        //Definir el NavController
        val navController = findNavController(R.id.nav_host_fragment)

        //Configurar el TabBar
        val appBarConfiguration = AppBarConfiguration(setOf(
            R.id.localizationFragment))
        setSupportActionBar(binding.toolbar)

        //Configuramos la navegación con la TabBar y el NavController
        setupActionBarWithNavController(navController, appBarConfiguration)

        //Configuramos la navegación con la BottomView y el NavController
        binding.navView.setupWithNavController(navController)
    }
}