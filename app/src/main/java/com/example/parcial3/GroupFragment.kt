package com.example.parcial3

import android.Manifest
import android.app.Activity
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.example.parcial3.databinding.ActivityMainBinding
import com.example.parcial3.databinding.FragmentGroupBinding

import kotlinx.android.synthetic.main.fragment_group.*


class GroupFragment : Fragment() {



    val REQUEST_CODE_GALLERY=1
    val REQUEST_CODE_CAMERA=2
    val requestLocationPermissionCode = 10

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentGroupBinding>(inflater, R.layout.fragment_group, container, false)


        binding.imageButton.setOnClickListener {
            imagenChange()

        }
        binding.imageButton2.setOnClickListener {
            imagenChange()
        }

        binding.imageButton3.setOnClickListener {
            imagenChange()
        }

        binding.map1Button.setOnClickListener {
            val mapInteFragment = MapIntegrantFragment()
            mapInteFragment.setIntegrante("Yor")
            val transaction = requireActivity().supportFragmentManager.beginTransaction()
            transaction.replace(id, mapInteFragment)
            transaction.commit()

        }

        binding.map2Button.setOnClickListener {
            val mapInteFragment = MapIntegrantFragment()
            mapInteFragment.setIntegrante("Yami")
            val transaction = requireActivity().supportFragmentManager.beginTransaction()
            transaction.replace(id, mapInteFragment)
            transaction.commit()
        }

        binding.map3Button.setOnClickListener {
            val mapInteFragment = MapIntegrantFragment()
            mapInteFragment.setIntegrante("Lau")
            val transaction = requireActivity().supportFragmentManager.beginTransaction()
            transaction.replace(id, mapInteFragment)
            transaction.commit()

        }
        return binding.root
    }


    private fun imagenChange() {
        val buldier = AlertDialog.Builder(requireActivity())
        buldier.setTitle("Editar camara desde:")
        buldier.setItems(arrayOf(
            "Galeria",
            "Camara"
        )) { dialog, position ->
            when(position){
                0 -> pickerFromGallery()
                1 -> selectFromCamera()
            }
        }
        buldier.setNegativeButton("Cancelar", null)
        buldier.show()
    }

    private fun pickerFromGallery(){
        val intent = Intent(Intent.ACTION_PICK)
        intent.type="image/*"
        startActivityForResult(intent, REQUEST_CODE_GALLERY)
    }

    private fun selectFromCamera(){
        val intent= Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, REQUEST_CODE_CAMERA)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            REQUEST_CODE_GALLERY ->{
                if (resultCode== RESULT_OK){
                    val uri= data?.data

                    imageButton.setImageURI(uri)
                }


            }
            REQUEST_CODE_CAMERA ->{
                if (resultCode== RESULT_OK){
                    val bitmap = data?.extras?.get("data") as Bitmap
                    imageButton.setImageBitmap(bitmap)
                }

            }
        }
    }

    private fun validatePermission() {
        if(ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            nextStep()
        } else {
            requestPermission()
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), requestLocationPermissionCode)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == requestLocationPermissionCode) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                nextStep()
            } else {
                Toast.makeText(requireContext(), "El permiso es necesario", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun nextStep() {
        val locationFragment = MapFragment()
        locationFragment.setIntegrante("Yor")
        val intent = Intent(requireActivity(), TabActivity::class.java)
        startActivity(intent)
    }




}