package com.example.parcial3

import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.OnSuccessListener


class MapIntegrantFragment :  SupportMapFragment(), OnMapReadyCallback, OnSuccessListener<Location>,
        GoogleMap.OnMapLongClickListener {

    var integrante2: String =""

    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())
        getMapAsync(this)
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(p0: GoogleMap) {
        this.map = p0
        this.map.setOnMapLongClickListener(this)
        fusedLocationClient.lastLocation.addOnSuccessListener(this)
    }

    override fun onSuccess(p0: Location) {
        if(integrante2.equals("Yor")){
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(6.217 , -75.567), 11f))
            map.addMarker(MarkerOptions().position(LatLng(6.217, -75.567)).title("Posición Actual"))
        }else if (integrante2.equals("Yami")){
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(8.75 , -75.883), 11f))
            map.addMarker(MarkerOptions().position(LatLng(8.75, -75.883)).title("Posición Actual"))
        }else  if (integrante2.equals("Lau")) {
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(3.42158, -76.5205), 11f))
                map.addMarker(MarkerOptions().position(LatLng(3.42158, -76.5205)).title("Posición Actual"))
            }
        }





    override fun onMapLongClick(p0: LatLng) {

            map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(p0.latitude, p0.longitude), 17f))
            map.addMarker(MarkerOptions().position(LatLng(p0.latitude, p0.longitude)).title("Posición Actual"))
    }

    public fun setIntegrante(inte:String){
        integrante2=inte
    }
}