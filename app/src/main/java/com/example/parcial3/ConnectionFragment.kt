package com.example.parcial3

import android.content.Intent
import android.os.Bundle

import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.parcial3.databinding.ActivityMainBinding

import com.example.parcial3.databinding.FragmentConnectionBinding
import com.example.parcial3.model.Country
import com.example.parcial3.model.CountryItem
import com.example.parcial3.model.RetrofitInstance

import com.xwray.groupie.GroupieAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ConnectionFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentConnectionBinding>(inflater, R.layout.fragment_connection, container, false)

        //Se puede manipular el binding
        binding.countriesRecyclerView.layoutManager = LinearLayoutManager(requireContext())


        val adapter = GroupieAdapter()
        binding.countriesRecyclerView.adapter = adapter

        val activity = this

        RetrofitInstance().covidApi().getCountries().enqueue(object :
            Callback<List<Country>> {
            override fun onResponse(call: Call<List<Country>>, response: Response<List<Country>>) {
                response.body()?.let { list ->
                    val countriesItem: List<CountryItem> = list.sortedBy { it.infectionRisk }.map { CountryItem(it) { code ->
                        val intent = Intent(requireActivity(), CasesActivity::class.java)
                        intent.putExtra("code", code)
                        requireActivity().startActivity(intent)
                    }
                    }
                    adapter.update(countriesItem)
                } ?: run {
                    if (response.code() == 401) {
                        Toast.makeText(requireActivity(), "El servicio no tiene el token", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(requireActivity(), "El servicio ha fallado", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onFailure(call: Call<List<Country>>, t: Throwable) {
                Toast.makeText(requireActivity(), "El servicio ha fallado", Toast.LENGTH_SHORT).show()
            }

        })
        return binding.root


    }
}











