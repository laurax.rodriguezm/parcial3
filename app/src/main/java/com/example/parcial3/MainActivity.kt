package com.example.parcial3

import android.graphics.Color.blue
import android.graphics.drawable.DrawableContainer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.parcial3.databinding.ActivityMainBinding
import com.example.parcial3.databinding.ActivityTabBinding
import com.google.android.material.navigation.NavigationView

class MainActivity : AppCompatActivity() {

    lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)


        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val navView= findViewById<NavigationView>(R.id.nav_view)
        val drawerLayout= findViewById<DrawerLayout>(R.id.drawer_layout)
        val navController = findNavController(R.id.nav_host_fragment)

        setSupportActionBar(toolbar)

        appBarConfiguration = AppBarConfiguration(setOf(R.id.connectionFragment,R.id.groupFragment,R.id.localizationFragment), drawerLayout)
        setupActionBarWithNavController(navController,appBarConfiguration)

        navView.setupWithNavController(navController)
    }

    override fun onSupportNavigateUp():Boolean{
        val navController= findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}